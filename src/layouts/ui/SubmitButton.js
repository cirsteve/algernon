import React, { Component, Fragment } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button';
import Dialog from '../common/Dialog'

class Deposit extends Component {
  constructor (props, context) {
    super(props)

    this.state = {
      openInfo: false,
      trxIdx: null
    }
  }

  componentDidUpdate (prevProps) {
    if (this.state.openInfo && this.state.hashId) {
      const {trxs, transactions, transactionStack} = this.props
      const trxIdx = trxs[this.state.hashId]
      if (trxIdx !== undefined && transactionStack[trxIdx] && transactions[transactionStack[trxIdx]].status === 'success') {
        this.setState({...this.state, openInfo: false, trxIdx: null})
      }
    }
  }

  submit = () => {
    const {submit} = this.props
    const trxIdx = submit()
    this.setState(...this.state, trxIdx, openInfo: true)
  }


  render () {
    const {text, transactions, trxs, transactionStack} = this.props
    const {trxIdx, hashId, openInfo} = this.state

    let content = 'Submit your transaction via MetaMask'
    let trxHash, trx
    if (trxIdx) {
      trxHash = transactionStack[trxIdx]
      if (trxHash) {
        content = 'Your transaction is pending'
      }
    }
      <Fragment>
        <Button variant="contained" onClick={this.submit} color="primary">
          {text}
        </Button>
        <Dialog
          dialogTitle=''
          open={openInfo}
          handleClose={this.updateShowingInfo.bind(this, false)}
          content={content} />
      </Fragment>
    )
  }
}

Deposit.contextTypes = {
  drizzle: PropTypes.object
}

const mapState = state => {
  return {
    trxs: state.trxs.trxs,
    transactionStack: state.transactionStack,
    transactions: state.transactions

  }
}

const mapDispatch = (dispatch) => {
    return {
        deposit: (approve, deposit) => dispatch({type: 'DEPOSIT_TOKENS', payload: {approve, deposit}})
    };
}
export default drizzleConnect(Deposit, mapState, mapDispatch);
