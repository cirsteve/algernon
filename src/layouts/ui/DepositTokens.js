import React, { Component, Fragment } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import Dialog from '../common/Dialog'

class Deposit extends Component {
  constructor (props, context) {
    super(props)

    this.state = {
      openInfo: false,
      hashId: null
    }
  }

  componentDidUpdate (prevProps) {
    if (this.state.openInfo && this.state.hashId) {
      const {trxs, transactions, transactionStack} = this.props
      const trxIdx = trxs[this.state.hashId]
      if (trxIdx !== undefined && transactionStack[trxIdx] && transactions[transactionStack[trxIdx]].status === 'success') {
        this.setState({...this.state, openInfo: false, hashId: null})
      }
    }
  }

  close = () => this.setState({...this.state, openInfo: false, hashId: null})

  submit = () => {
    const {value} = this.props
    const hashId = Math.ceil(Math.random() * 1000000000000)
    this.props.deposit(
      hashId,
      this.context.drizzle.contracts.AlgerToken.methods.approve.cacheSend.bind(this, this.context.drizzle.contracts.Algernon.address, value),
      this.context.drizzle.contracts.Algernon.methods.depositTokens.cacheSend.bind(this, value)
    )
    this.setState({...this.state, hashId, openInfo: true})
  }


  render () {
    const {transactions, trxs, transactionStack} = this.props
    const { hashId, openInfo } = this.state
    let content = 'Submit your transaction via MetaMask'

    if (hashId && trxs[hashId] === 1) {
      content = 'Your transaction is pending'
    }

    return (
      <Fragment>
        <Button variant="contained" onClick={this.submit} color="primary">
          Deposit Tokens
        </Button>
        <Dialog
          dialogTitle=''
          open={openInfo}
          handleClose={this.close}
          content={content} />
      </Fragment>
    )
  }
}

Deposit.contextTypes = {
  drizzle: PropTypes.object
}

const mapState = state => {
  return {
    trxs: state.trxs.trxs,
    transactionStack: state.transactionStack,
    transactions: state.transactions

  }
}

const mapDispatch = (dispatch) => {
    return {
        deposit: (hashId, approve, deposit) => dispatch({type: 'DEPOSIT_TOKENS', payload: {hashId, approve, deposit}})
    };
}
export default drizzleConnect(Deposit, mapState, mapDispatch);
