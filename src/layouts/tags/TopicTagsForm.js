import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { drizzleConnect } from 'drizzle-react'
import {difference, findIndex, reverse} from 'lodash'
import Select from '../common/forms/Select'
import IconSubmit from '../common/forms/IconSubmit'
import Dialog from '../common/Dialog'
import Loading from '../common/Loading'

class Form extends Component {
  constructor (props, context) {
    super(props)

    this.state = {
      openInfo: false,
      tagIds: null,
      trxIdx: null
    }
  }

  componentDidUpdate (prevProps) {
    const {openInfo, trxIdx} = this.state
    if (openInfo && Number.isInteger(trxIdx)) {
      const { transactions, transactionStack} = this.props
      const trxHash =  transactionStack[trxIdx]
      if (trxHash && transactions[trxHash].status === 'success') {
        this.close()
      }
    }
  }

  updateTag = (e) => this.setState({...this.state, tagIds: e.target.value})

  onSubmit = () => {
    const {topicId} = this.props
    const removedIds = reverse(difference(this.props.tagIds, this.state.tagIds))
    const removedIdxs = removedIds.map(id => findIndex(this.props.tagIds, i=> i === id))
    const topicTagIdxs = removedIds.map(id => findIndex(this.props.tagTopicIds[id], i => i === topicId))
    const addedIds = difference(this.state.tagIds, this.props.tagIds, this.props.tagTopicIds)
    console.log('tag update: ', topicId, addedIds, removedIds, removedIdxs, topicTagIdxs, this.props.tagTopicIds)

    const trxIdx = this.context.drizzle.contracts.Algernon.methods.updateTopicTags.cacheSend(
      addedIds,
      removedIds,
      removedIdxs,
      topicTagIdxs,
      topicId
    )

    this.setState({...this.state, trxIdx, openInfo:true})
  }

  clear = () => {
    this.setState({...this.state, tagIds:null})
  }

  close = () => {
    this.setState({
      openInfo: false,
      tagIds: null,
      trxIdx: null
    })
    this.props.onCancel()
  }

  render () {
    const { transactionStack } = this.props
    const {openInfo, tagIds, trxIdx} = this.state

    const content = Number.isInteger(trxIdx) && transactionStack[trxIdx] ?
      <div>Your transaction is pending<Loading /></div>
      :
      'Complete Your Submission wtih MetaMask'
    return (
      <div>
        <IconSubmit onSubmit={this.onSubmit} onCancel={this.clear} />
        <Select multiple options={this.props.options} onChange={this.updateTag} value={tagIds || this.props.tagIds} />
        <Dialog
          dialogTitle=''
          open={openInfo}
          handleClose={this.close}
          content={content} />
      </div>
    )
  }
}

Form.contextTypes = {
  drizzle: PropTypes.object
}
const mapState = state => {
  return {
    trxs: state.trxs.trxs,
    transactionStack: state.transactionStack,
    transactions: state.transactions
  }
}

export default drizzleConnect(Form, mapState)
