import React, { Component } from 'react'
import { drizzleConnect } from 'drizzle-react'
import PropTypes from 'prop-types'
import IconSubmit from '../common/forms/IconSubmit'
import Text from '../common/forms/Text'
import Button from '../common/forms/Button'
import Dialog from '../common/Dialog'
import { getTrxHashes } from '../../util/transactions'
import Loading from '../common/Loading'

class Section extends Component {
  constructor (props, context) {
    super(props)

    this.state = {
      amount: 0,
      deposit: false,
      withdraw: false,
      hashId: null,
      openInfo: false,
      trxIdx: null
    }
  }

  componentDidUpdate = () => {
    const {transactions, trxs, transactionStack} = this.props
    const {openInfo, hashId, trxIdx} = this.state

    if (openInfo && transactionStack[trxIdx] && transactions[transactionStack[trxIdx]].status === 'success' ) {
      this.clearInput(null, false)
    } else if ( openInfo && hashId && trxs[hashId]) {
      const submitTrxs = getTrxHashes(transactionStack, trxs[hashId])

      if (submitTrxs.length === trxs[hashId].length && transactions[submitTrxs.pop()].status === 'success') {
        this.clearInput(null, false)
      }
    }
  }

  close = () => this.setState({...this.state, openInfo: false})

  clearInput = (hashId, openInfo, trxIdx) => this.setState({...this.state, amount: 0, deposit: false, withdraw: false, hashId, openInfo, trxIdx})

  updateState = (field, e) => this.setState({...this.state, [field]: e.target ? e.target.value : e})

  submit = () => {
    this.context.drizzle.contracts.AlgerToken.methods.mint.cacheSend(this.state.to, this.state.amount)
    this.clearInput()
  }

  deposit = () => {
    const hashId = Math.ceil(Math.random() * 1000000000000)
    this.props.deposit(
      hashId,
      this.context.drizzle.contracts.AlgerToken.methods.approve.cacheSend.bind(this, this.context.drizzle.contracts.Algernon.address, this.state.amount),
      this.context.drizzle.contracts.Algernon.methods.depositTokens.cacheSend.bind(this, this.state.amount)
    )
    this.clearInput(hashId, true)
  }

  withdraw = () => {
    const trxIdx = this.context.drizzle.contracts.Algernon.methods.withdrawTokens.cacheSend(this.state.amount)
    this.clearInput(null, true, trxIdx)
  }

  render () {
    const {transactions, trxs, transactionStack} = this.props
    const { hashId, trxIdx, openInfo } = this.state
    let content = 'Submit your transaction via MetaMask'
    console.log('lt: ', transactionStack, transactions, trxIdx)
    if (hashId && trxs[hashId]) {
      const latestTrx = getTrxHashes(transactionStack, trxs[hashId]).pop()
      if (!!latestTrx && latestTrx.status !== 'success') {
        content = <div>Your transaction is pending<Loading /></div>
      }
    } else if (Number.isInteger(trxIdx) && transactions[transactionStack[trxIdx]]) {
      content = <div>Your transaction is pending<Loading /></div>
    }
    return (
      <div>
        <h2>Wallet Token Balance: {this.props.balance}</h2>
        <h2>Algernon Token Balance: {this.props.contractBalance}</h2>
        {this.state.deposit || this.state.withdraw ?
          <Text onChange={this.updateState.bind(this, 'amount')} value={this.state.amount} label="amount" type="number" />
          :
          null
        }
        {this.state.deposit ?
          <IconSubmit onCancel={this.clearInput} onSubmit={this.deposit} />
          :
          this.state.withdraw ?
            <IconSubmit onCancel={this.clearInput} onSubmit={this.withdraw} />
            :
            <div>
              <span style={{marginRight: '1em'}}>
                <Button onClick={this.updateState.bind(this, 'deposit', true)} text="Deposit" />
              </span>
              <Button onClick={this.updateState.bind(this, 'withdraw', true)} text="Withdraw" />
            </div>
        }
        <Dialog
          dialogTitle=''
          open={openInfo}
          handleClose={this.close}
          content={content} />
      </div>
    )
  }
}

Section.contextTypes = {
  drizzle: PropTypes.object
}


const mapState = state => {
  return {
    trxs: state.trxs.trxs,
    transactionStack: state.transactionStack,
    transactions: state.transactions

  }
}

const mapDispatch = (dispatch) => {
    return {
        deposit: (hashId, approve, deposit) => dispatch({type: 'DEPOSIT_TOKENS', payload: {hashId, approve, deposit}})
    };
}
export default drizzleConnect(Section, mapState, mapDispatch);
