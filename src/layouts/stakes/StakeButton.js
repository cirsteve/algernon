import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { drizzleConnect } from 'drizzle-react'
import { withStyles } from '@material-ui/core/styles';
import Text from '../common/forms/Text'
import Loading from '../common/Loading'
import IconSubmit from '../common/forms/IconSubmit'
import Fab from '@material-ui/core/Fab';
import PlusIcon from '@material-ui/icons/AddCircleOutline'
import MinusIcon from '@material-ui/icons/RemoveCircleOutline'
import Dialog from '../common/Dialog'

const UPDATE_TYPES = {
  ADD: 'ADD',
  INCREASE: 'INCREASE',
  DECREASE: 'DECREASE'
}

const styles = theme => ({
  fab: {
    margin: theme.spacing.unit,
  },
  button: {
    cursor: 'pointer'
  },
  dialog: {
    textAlign: 'center'
  }
});

class Form extends Component {
  constructor (props, context) {
    super(props)

    this.state = {
      showForm: false,
      amount:0,
      updateType: UPDATE_TYPES.ADD,
      trxIdx: null
    }
  }

  componentDidUpdate = () => {
    const {transactions, transactionStack} = this.props
    const {showForm, trxIdx} = this.state

    if (showForm && transactionStack[trxIdx] && transactions[transactionStack[trxIdx]].status === 'success' ) {
      this.close()
    }
  }

  close = () => this.setState({
    showForm: false,
    amount:0,
    updateType: UPDATE_TYPES.ADD,
    trxIdx: null
  })

  updateAmount = (e) => this.setState({...this.state, amount: parseInt(e.target.value)})

  showForm = (type) => this.setState({...this.state, showForm: true, updateType: type})

  toggleDialog = showForm => this.setState({...this.state, showForm})

  closeForm = () => this.setState({
    showForm: false,
    amount:0,
    updateType: UPDATE_TYPES.ADD,
    trxIdx: null
  })

  submitAdd = () => {
    const {topicId, tagId} = this.props
    const {amount} = this.state

    const trxIdx = this.context.drizzle.contracts.Algernon.methods.addStake.cacheSend(
      topicId,
      parseInt(tagId),
      parseInt(amount)
    )

    this.setState({...this.state, trxIdx, amount:0})
  }

  submitIncrease = () => {
    const {userStake} = this.props
    const {amount} = this.state

    const trxIdx = this.context.drizzle.contracts.Algernon.methods.increaseStake.cacheSend(
      userStake[0],
      amount
    )

    this.setState({...this.state, trxIdx, amount:0})
  }

  submitDecrease = () => {
    const {userStake} = this.props
    const {amount} = this.state

    const trxIdx = this.context.drizzle.contracts.Algernon.methods.reduceStake.cacheSend(
      userStake[0],
      amount
    )

    this.setState({...this.state, trxIdx, amount:0})
  }

  onCancel = () => this.setState({...this.state, updateType: UPDATE_TYPES.ADD, amount:0})

  render () {
    const {userStake, stakeTotal, topicTitle, tag, classes, transactionStack} = this.props
    const {showForm, trxIdx, amount, updateType} = this.state
    let trxHash = null

    let onSubmit, description;
    if (Number.isInteger(trxIdx)) {
      trxHash = transactionStack[trxIdx]
      if (trxHash) {
        description = <span><h4>Your transaction is pending</h4><Loading /></span>
      } else {
        description = <h4>Submit your transaction with MetaMask</h4>
      }
    } else if (!userStake) {
      onSubmit = this.submitAdd
      description =
        <span>
          Stake {amount} tokens to the combination of the tag <h3>{tag}</h3> and the topic <h3>{topicTitle}</h3>
        </span>
    } else if  (updateType === UPDATE_TYPES.INCREASE) {
      onSubmit = this.submitIncrease
      description =
        <div>
          <span>
            Increase your stake by {amount} token to the combination of the tag
          </span>
          <h3>
            {tag}
          </h3>
          <span>
            and the topic
          </span>
          <h3>
            {topicTitle}
          </h3>
        </div>
    } else if (updateType === UPDATE_TYPES.DECREASE) {
      onSubmit = this.submitDecrease
      description =
        <div>
          <span>
            Decrease your stake by {amount} token to the combination of the tag
          </span>
          <h3>
            {tag}
          </h3>
          <span>
            and the topic
          </span>
          <h3>
            {topicTitle}
          </h3>
        </div>
    } else {
      description =
        <div>
          <div>
            <Fab onClick={this.showForm.bind(this, UPDATE_TYPES.DECREASE)} color="error" aria-label="Edit" className={classes.fab}>
              <MinusIcon />
            </Fab>
            <Fab onClick={this.showForm.bind(this, UPDATE_TYPES.INCREASE)} color="error" aria-label="Edit" className={classes.fab}>
              <PlusIcon />
            </Fab>

          </div>
          <span>
            on the combination of the tag <h3>{tag}</h3> and the topic <h3>{topicTitle}</h3>
          </span>
        </div>
    }

    const updateAmount = userStake ?
      updateType === UPDATE_TYPES.INCREASE ?
        parseInt(userStake[1]) + amount
        :
        parseInt(userStake[1]) - amount
      :
      null

    const updateInfo = updateAmount ?
      <div>
        {userStake[1]}
        <div>
          {UPDATE_TYPES.INCREASE === updateType ?
            '+'
            :
            '-'
          }
          {amount}
        </div>
        <div>
          {updateAmount}
        </div>
      </div>
      :
      null

    const form = userStake && updateType === UPDATE_TYPES.ADD ?
      <div>
        Your current stake is
        <h2>
          {userStake[1]} tokens
        </h2>
      </div>
      :
      <div>
        <Text value={amount} onChange={this.updateAmount} fullWidth={false} />
        {updateInfo}
        <IconSubmit onSubmit={onSubmit} onCancel={this.onCancel} />
      </div>

    const content =
      <div className={classes.dialog}>
        {trxHash !== null ? null : form}
        <p>
          {description}
        </p>
      </div>

    return (
      <span>
        <span className={classes.button} onClick={this.toggleDialog.bind(this, true)}>
          {stakeTotal}
        </span>

        <Dialog
          dialogTitle=''
          open={showForm}
          handleClose={this.close}
          content={content} />

      </span>
    )
  }
}

Form.contextTypes = {
  drizzle: PropTypes.object
}

const mapState = state => {
  return {
    transactionStack: state.transactionStack,
    transactions: state.transactions

  }
}

export default withStyles(styles)(drizzleConnect(Form, mapState))
