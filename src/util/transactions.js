export const getTrxState = (hashes, trxs) => hashes.map(
  hash => trxs[hash] ? trxs[hash].status === 'success' ? 2 : 1: 0
)

export const getTrxHashes = (transactions, idxs) => idxs.map(
  idx => transactions[idx]).filter(idx => idx
  )
