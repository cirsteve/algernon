import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { drizzleReducers } from 'drizzle'
import dataReducer from './app/reducers/data'
import tagsReducer from './app/reducers/tags'
import trxsReducer from './app/reducers/trxs'

const reducer = combineReducers({
  data: dataReducer,
  routing: routerReducer,
  tags: tagsReducer,
  trxs: trxsReducer,
  ...drizzleReducers
})

export default reducer
