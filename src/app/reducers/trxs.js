import produce from 'immer'
const initialState = {
    trxs: {}
}

export default (state = initialState, action) => {
  return produce(state, draftState => {
    switch (action.type) {
        case 'TRX_SENT':
          draftState.trxs[action.payload.hashId] = action.payload.trxIdx
          break
        default:
          return state;
    }
  })
}
